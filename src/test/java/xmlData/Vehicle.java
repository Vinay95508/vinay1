package xmlData;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Vehicle {

	public static void main(String[] args)
			throws ParserConfigurationException, MalformedURLException, SAXException, IOException {
		DocumentBuilderFactory docfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder xmlBluid = docfac.newDocumentBuilder();
		Document xmlDoc = xmlBluid.parse(new URL("https://www.sodineg.com/articles/apistock/lg:en").openStream());
		xmlDoc.getDocumentElement().normalize();
		System.out.println(xmlDoc.getDocumentElement().getNodeName());
		NodeList list = xmlDoc.getElementsByTagName("vehicle");
		// Node n;
		for (int i = 1; i < list.getLength(); i++) {
			Node n = list.item(i);
			Element ele = (Element) n;
			System.out.println("vehicle: " + i);
			//getDescription(ele);
			System.out.println(getOptionalField(ele));

			System.out.println("references " + getAdId(ele));
			
			// System.out.println("nomenclature " +getVehicle(ele, "nomenclature"));
			// System.out.println("manufacturer" +getVehicle(ele, "manufacturer"));
			// System.out.println("model " + getVehicle(ele, "model"));
			// System.out.println("description " + getVehicle(ele, "description"));
			// System.out.println("hours " + getVehicle(ele, "hours"));
			// System.out.println("year " + getVehicle(ele, "year"));
			// System.out.println("pv" + getVehicle(ele, "pv"));
			// System.out.println("release-at " + getVehicle(ele, "release-at"));
			// System.out
			// .println("serial-number " + getVehicle(ele, "serial-number"));
			// System.out.println("demonstration-model "
			// + getVehicle(ele, "demonstration-model"));
			// System.out.println("price " + getVehicle(ele, "price"));
			//System.out.println("price " + getBodyCategory(ele, "price","currency"));
			// System.out.println(getPriceType(ele));
			// System.out.println(getImages(ele, xmlDoc));
			// System.out.println(getDescription(ele));
			//System.out.println(getOptionalField(ele));
			// System.out.println("child category "+getChildCategory(ele, "category"));
			// System.out.println("sub category = "+getSubCategory(ele, "category"));
			// System.out.println("category = "+getCategory(ele, "category"));
			// System.out.println("internet price " + getVehicle(ele, "internet"));
			// System.out.println("body " + getBodyCategory(ele, "body","name"));
			// System.out.println(
			// "category " + getBodyCategory(ele, "category","name"));

			// NodeList images = ele.getElementsByTagName("media");
			// for (int j = 0; j < images.getLength(); j++) {
			// Node img_Node = images.item(j);
			// Element img_Element = (Element) img_Node;
			// System.out.println("media image" + j + "=" +
			// img_Element.getAttribute("src"));
			// }
			//System.out.println(getVehicle(ele, "mileage"));
			///System.out.println(getVehicle(ele, "year"));
			//System.out.println(getVehicle(ele, "hours"));
			//System.out.println(getCondition(ele));
			System.out.println(getDescription(ele));


			System.out.println("...............................................................");

		}
	}

	public static String getVehicle(Element ele, String tagNmae) {
		String vehicleinfo = "null";
		try {
			vehicleinfo = ele.getElementsByTagName(tagNmae).item(0).getTextContent();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return vehicleinfo;

	}

	public static String getBodyCategory(Element ele, String tagName, String attributeName) {
		String name = null;
		try {
			name = ((Element) ele.getElementsByTagName(tagName).item(0)).getAttribute(attributeName);
		} catch (Exception e) {

		}
		return name;
	}
	
	public static String getAdId(Element ele)
	{
		return ele.getAttribute("ref");
	}

	public static String getCategory(Element ele, String tagName) {
		String category = ((Element) ele.getElementsByTagName(tagName).item(0)).getAttribute("name");
		String[] split = category.split("[|]");
		return category = split[0];
	}

	public static String getSubCategory(Element ele, String tagName) {
		String sub_Category = ((Element) ele.getElementsByTagName(tagName).item(0)).getAttribute("name");
		String[] split = sub_Category.split("[|]");
		try {
			sub_Category = split[1];
		} catch (Exception e) {
			// TODO: handle exception
		}
		return sub_Category;
	}

	public static String getChildCategory(Element ele, String tagName) {
		String child_Category = ((Element) ele.getElementsByTagName(tagName).item(0)).getAttribute("name");

		String[] split = child_Category.split("[|]");
		if (split.length > 2) {
			child_Category = split[2];
		} else {
			child_Category = "null";
		}

		return child_Category;
	}

	public static String getPriceType(Element ele) {
		String price = getVehicle(ele, "price");
		String price_Type = "";
		if (price.equals("null")) {
			price_Type = "On Request";
		} else {
			price_Type = "fixed";
		}
		return price_Type;
	}

	public static String getImages(Element ele, Document xmlDoc) {

		String image = "";
		

		NodeList images = ele.getElementsByTagName("media");
		for (int j = 0; j < images.getLength(); j++) {
			Node img_Node = images.item(j);
			Element img_Element = (Element) img_Node;
			String img = img_Element.getAttribute("src");
			image = image + img + " <next> ";

		}

		return image;
	}

	public static String getDescription(Element ele) {
		JSONObject desc = new JSONObject();
		String details= "";
		desc.put("en", getVehicle(ele, "description"));
		JSONObject extra_ad_attribute = new JSONObject();
		NodeList list = ele.getElementsByTagName("feature");

		for (int i = 0; i < list.getLength(); i++) {
			Node n = list.item(i);
			Element ele1 = (Element) n;
			String name = ele1.getAttribute("name");// ;
			
			String value ="";// ele1.getAttribute("value");
			
			String weight = ele1.getTextContent()+" "+ele1.getAttribute("unit");
			String unit = "";
			unit = getBodyCategory(ele1, "feature", "unit");
			String valueText = "";
			valueText = getBodyCategory(ele1, "feature", "value");
			
			
			if(name.equals("mast_type")||name.equals("dump_make"))
			{
				value = ele1.getAttribute("value").replaceAll("\\W", "").trim();
				
				/*try {
					if (name.equals("ptac")) {
						System.out.println(name+","+weight);
						extra_ad_attribute.put("Weight", weight);
						continue;
					} else {
						
						extra_ad_attribute.put(name, value);
						
						if(unit.equals("kg"))
						extra_ad_attribute.put("Weight", weight);
					}

				} catch (Exception e) {
					
				}*/extra_ad_attribute.put(name, value);

			}
			else
			{
				continue;
			}

			System.out.println(name+","+value);
			/*try {
				if (name.equals("ptacConfiguration")) {
					optional_field.put("Weight", weight);
					continue;
				} else {
					optional_field.put(name, value);
					if(unit.equals("kg"))
					optional_field.put("Weight", weight);
				}

			} catch (Exception e) {
				// TODO: handle exception
			}*/

		}
		String optional = extra_ad_attribute.toString();
		desc.put("extra_ad_attributes", optional);
		
		details=desc.toString();
		return details;
	}
	
	public static String getOptionalField(Element ele) {
		JSONObject optional_field = new JSONObject();
		NodeList list = ele.getElementsByTagName("feature");
		System.out.println(list.getLength());

		for (int i = 0; i < list.getLength(); i++) {
			Node n = list.item(i);
			Element ele1 = (Element) n;
			String name = ele1.getAttribute("name");// ;
			
			String value =ele1.getAttribute("value");
			
			String weight = ele1.getTextContent()+" "+ele1.getAttribute("unit");
			String unit = "";
			unit = getBodyCategory(ele1, "feature", "unit");
			String valueText = "";
			valueText = getBodyCategory(ele1, "feature", "value");
			
			if(value.equals("Sur pneus"))
			{
				value="On tires"; 		
			}
			else if(value.equals("Sur chenilles"))
				value="On tracks";

			
			
			
			if(name.equals("driving_wheels_number"))
			{
				name="no-of-wheels";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("axles"))
			{
				name="axleConfiguration";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("type_PELLE_STANDARD")||name.equals("type_CHARGE"))
			{
				name="undercarriage-type";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("euro_norm"))
			{
				name="emissionLevel";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("rear_tyre_condition"))
			{
				name="rear-axle-tire-condition-pct";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("front_tyre_condition"))
			{
				name="front-axle-tire-condition-pct";
				//value = ele1.getAttribute("value");
			}
			else if(name.equals("ptac"))
			{
				name="Weight";
				//value=ele1.getTextContent()+" "+ele1.getAttribute("unit");
			}
			else
			{
				continue;
			}
			
			//System.out.println(name+","+value);
			optional_field.put(name, value);

		}
		String optional = optional_field.toString();
		optional = optional.replaceAll(",", " <next> ");
		return optional;
	}
	public static String getCondition(Element ele)
	{
		String condition="";
		String conditionCheck=ele.getAttribute("used");
		if(conditionCheck.equalsIgnoreCase("true"))
		{
			condition="used";
		}else
		{
			condition="new";
		}
		return condition;
		
	}

	
}
