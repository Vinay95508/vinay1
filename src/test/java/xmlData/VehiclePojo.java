package xmlData;

public class VehiclePojo {
	private int user_id;
	private String ad_id_from_site;
	private String category;
	private String sub_category;
	private String child_category;
	private String make;
	private String model;
	private String make_year;
	private String millage;
	private String moto_hours;
	private String price;
	private String currency_type;
	private String price_type;
	private String condition;
	private String images;
	private String description;
	private String optional_fields;
	private String site_name;
	private String ad_status;
	public VehiclePojo() {
		
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getAd_id_from_site() {
		return ad_id_from_site;
	}
	public void setAd_id_from_site(String ad_id_from_site) {
		this.ad_id_from_site = ad_id_from_site;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSub_category() {
		return sub_category;
	}
	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}
	public String getChild_category() {
		return child_category;
	}
	public void setChild_category(String child_category) {
		this.child_category = child_category;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMake_year() {
		return make_year;
	}
	public void setMake_year(String make_year) {
		this.make_year = make_year;
	}
	public String getMillage() {
		return millage;
	}
	public void setMillage(String millage) {
		this.millage = millage;
	}
	public String getMoto_hours() {
		return moto_hours;
	}
	public void setMoto_hours(String moto_hours) {
		this.moto_hours = moto_hours;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCurrency_type() {
		return currency_type;
	}
	public void setCurrency_type(String currency_type) {
		this.currency_type = currency_type;
	}
	public String getPrice_type() {
		return price_type;
	}
	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOptional_fields() {
		return optional_fields;
	}
	public void setOptional_fields(String optional_fields) {
		this.optional_fields = optional_fields;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getAd_status() {
		return ad_status;
	}
	public void setAd_status(String ad_status) {
		this.ad_status = ad_status;
	}
	
	

}
