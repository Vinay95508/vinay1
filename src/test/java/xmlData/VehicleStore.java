package xmlData;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
 

public class VehicleStore {

	public static void main(String[] args)
			throws ParserConfigurationException, MalformedURLException, SAXException, IOException, SQLException, ClassNotFoundException {
		DocumentBuilderFactory docfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder xmlBluid = docfac.newDocumentBuilder();
		Document xmlDoc = xmlBluid.parse(new URL("https://www.sodineg.com/articles/apistock/lg:en").openStream());
		xmlDoc.getDocumentElement().normalize();
		System.out.println(xmlDoc.getDocumentElement().getNodeName());
		NodeList list = xmlDoc.getElementsByTagName("vehicle");
		DataBaseMethods.getDataBaseConnection();
		DataBaseMethods.getDataBaseDriver();
		PreparedStatement psmt = DataBaseMethods.getPreparedStatement();
		VehiclePojo pojo_Object= new VehiclePojo();
		for (int i = 0; i < list.getLength(); i++) {
			Node n = list.item(i);
			Element ele = (Element) n;
			System.out.println("vehicle: " + i);

			setter(ele, pojo_Object,xmlDoc);
			getter(psmt, pojo_Object);

			DataBaseMethods.insertValues(psmt, pojo_Object);

			System.out.println("...............................................................");

		}
		DataBaseMethods.close();

	}

	
	public static void getter(PreparedStatement psmt, VehiclePojo pojo_Object) throws SQLException {
		int i = 1;
		psmt.setInt(i++, pojo_Object.getUser_id());
		psmt.setString(i++, pojo_Object.getAd_id_from_site());
		psmt.setString(i++, pojo_Object.getCategory());
		psmt.setString(i++, pojo_Object.getSub_category());
		psmt.setString(i++, pojo_Object.getChild_category());
		psmt.setString(i++, pojo_Object.getMake());
		psmt.setString(i++, pojo_Object.getModel());
		psmt.setString(i++, pojo_Object.getMake_year());
		psmt.setString(i++, pojo_Object.getMillage());
		psmt.setString(i++, pojo_Object.getMoto_hours());
		psmt.setString(i++, pojo_Object.getPrice());
		psmt.setString(i++, pojo_Object.getCurrency_type());
		psmt.setString(i++, pojo_Object.getPrice_type());
		psmt.setString(i++, pojo_Object.getCondition());
		psmt.setString(i++, pojo_Object.getImages());
		psmt.setString(i++, pojo_Object.getDescription());
		psmt.setString(i++, pojo_Object.getOptional_fields());
		psmt.setString(i++, pojo_Object.getSite_name());
		psmt.setString(i++, pojo_Object.getAd_status());
		

	}
	public static void setter(Element ele, VehiclePojo pojo_Object,Document xmlDoc) {

		
		pojo_Object.setUser_id(1);
		pojo_Object.setAd_id_from_site(Vehicle.getAdId(ele));
		pojo_Object.setCategory(Vehicle.getCategory(ele, "category"));
		pojo_Object.setSub_category(Vehicle.getSubCategory(ele, "category"));
		pojo_Object.setChild_category(Vehicle.getChildCategory(ele, "category"));
		pojo_Object.setMake(Vehicle.getVehicle(ele, "manufacturer"));
		pojo_Object.setModel(Vehicle.getVehicle(ele, "model"));
		pojo_Object.setMake_year(Vehicle.getVehicle(ele, "year"));
		pojo_Object.setMillage(Vehicle.getVehicle(ele, "mileage"));
		pojo_Object.setMoto_hours(Vehicle.getVehicle(ele, "hours"));
		pojo_Object.setPrice(Vehicle.getVehicle(ele, "price"));
		pojo_Object.setCurrency_type(Vehicle.getBodyCategory(ele, "price","currency"));
		pojo_Object.setPrice_type(Vehicle.getPriceType(ele));
		pojo_Object.setCondition(Vehicle.getCondition(ele));
		pojo_Object.setImages(Vehicle.getImages(ele, xmlDoc));
		pojo_Object.setDescription(Vehicle.getDescription(ele));
		pojo_Object.setOptional_fields(Vehicle.getOptionalField(ele));
		pojo_Object.setSite_name("www.sodineg.com");
		pojo_Object.setAd_status("imported");
	}


}
